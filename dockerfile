# Usa una imagen base existente
FROM ubuntu:20.04

# Instala las dependencias y herramientas necesarias
RUN apt-get update && apt-get install -y python3

WORKDIR /app

COPY . .

# Ejecuta el comando para iniciar tu aplicación
CMD [ "python3", "task_four.py" ]