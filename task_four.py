def find_matching_pair(numbers, target_sum):
    for i in range(len(numbers) - 1):
        if numbers[i] + numbers [i + 1] == target_sum:
            return (numbers[i], numbers[i + 1])
    return None

    
    
    
def test_find_matching_pair():
    """
    Unit tests for the find_matching_pair function.
    """

    # Test case 1: Matching pair exists
    test_numbers = [1, 3, 5, 7]
    target_sum = 8
    expected_pair = (3, 5)
    assert find_matching_pair(test_numbers, target_sum) == expected_pair

    # Test case 2: No matching pair
    test_numbers = [1, 3, 5, 7]
    target_sum = 12
    expected_pair = (5,7)
    assert find_matching_pair(test_numbers, target_sum) == expected_pair

    # Test case 3: Empty list
    test_numbers = []
    target_sum = 5
    expected_pair = None
    assert find_matching_pair(test_numbers, target_sum) == expected_pair

    # Test case 4: Zero target sum
    test_numbers = [1, 2, 3]
    target_sum = 0
    expected_pair = None  # Consider returning a specific value for zero target sum (optional)
    assert find_matching_pair(test_numbers, target_sum) == expected_pair

    # Test case 5: Duplicate elements (optional)
    test_numbers = [1, 2, 2, 3]
    target_sum = 4
    expected_pair = (2, 2)  # May return different pairs with duplicates (clarify expected behavior)
    assert find_matching_pair(test_numbers, target_sum) == expected_pair

if __name__ == "__main__":
    test_find_matching_pair()
    print("All tests passed!")
